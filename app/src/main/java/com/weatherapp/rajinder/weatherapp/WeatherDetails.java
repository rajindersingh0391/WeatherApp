package com.weatherapp.rajinder.weatherapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.weatherapp.rajinder.weatherapp.adapters.WeatherAdapter;
import com.weatherapp.rajinder.weatherapp.model.WeatherObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class WeatherDetails extends AppCompatActivity {

    private ListView view;
    private WeatherAdapter adapter;
    private List<String> weatherTitleList;
    private List<String> weatherList;
    private List<WeatherObject> weatherObjectList;
    JSONObject jsonObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_details);

        view = (ListView) findViewById(R.id.listView);
        Intent intent = getIntent();
        try {
            String jsonString = intent.getStringExtra("jsonWeather");
            List<WeatherObject> list = getDataList(new JSONObject(jsonString));

            adapter = new WeatherAdapter(getApplicationContext(), list);
            view.setAdapter(adapter);
        } catch (JSONException e){
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.error),
                    Toast.LENGTH_LONG).show();
        }
    }

    //returns the list that gets populated in the View
    // NOTE: Clouds and Sunrise return numbers, not sure what they mean at this point so I left it as it is.
    private List<WeatherObject> getDataList(JSONObject jsonObj){
        try {
            weatherObjectList = new ArrayList<WeatherObject>();
            weatherTitleList = new ArrayList<String>();
            weatherList = new ArrayList<String>();

            Iterator<?> keys = jsonObj.keys();

            while( keys.hasNext() ) {
                String key = (String)keys.next();
                if ( jsonObj.get(key) instanceof Object ) {
                    Log.i("Myapp", key);
                    if("main".equalsIgnoreCase(key.toString())){
                        weatherTitleList.add("Temperature");
                        weatherList.add(jsonObj.getJSONObject("main").getString("temp") + " F");

                        weatherTitleList.add("Pressure");
                        weatherList.add(jsonObj.getJSONObject("main").getString("pressure") + " hpa");

                        weatherTitleList.add("Humidity");
                        weatherList.add(jsonObj.getJSONObject("main").getString("humidity") + "%");
                    }
                    if("weather".equalsIgnoreCase(key.toString())){

                        String obj = jsonObj.getJSONArray("weather").get(0).toString();
                        JSONObject jObj = new JSONObject(obj);
                        Iterator<?> weatherKeys = jObj.keys();

                        while(weatherKeys.hasNext()) {
                            String weatherKey = (String)weatherKeys.next();
                            if ( jObj.get(weatherKey) instanceof Object ) {
                                if (("main").equalsIgnoreCase(weatherKey.toString())) {
                                    weatherTitleList.add("Conditions");
                                    weatherList.add(jObj.getString("main"));
                                }
                            }
                        }
                    }
                    if("wind".equalsIgnoreCase(key.toString())){
                        weatherTitleList.add("Speed");
                        weatherList.add(jsonObj.getJSONObject("wind").getString("speed") + " m/s");
                    }
                    if("clouds".equalsIgnoreCase(key.toString())){
                        weatherTitleList.add("Clouds");
                        weatherList.add(jsonObj.getJSONObject("clouds").getString("all"));
                    }
                    if("sys".equalsIgnoreCase(key.toString())){
                        weatherTitleList.add("Sunrise");
                        weatherList.add(jsonObj.getJSONObject("sys").getString("sunrise"));
                    }
                    if("name".equalsIgnoreCase(key.toString())){
                        getSupportActionBar().setTitle(jsonObj.getString("name"));
                    }
                }
            }

            weatherObjectList.add(new WeatherObject(weatherTitleList,weatherList));
        }catch(JSONException e){
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.error),
                    Toast.LENGTH_LONG).show();
        }

        return weatherObjectList;
    }
}
