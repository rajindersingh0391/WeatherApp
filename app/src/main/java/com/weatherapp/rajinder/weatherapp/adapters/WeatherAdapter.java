package com.weatherapp.rajinder.weatherapp.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.weatherapp.rajinder.weatherapp.R;
import com.weatherapp.rajinder.weatherapp.model.WeatherObject;

import java.util.List;


public class WeatherAdapter extends BaseAdapter {

    private Context context;
    private List<String> weatherList;
    private List<String> weatherTitleList;
    List<WeatherObject> weatherObjectList;


    public WeatherAdapter(Context context,List<WeatherObject> weatherObjectList) {
        this.context = context;
        this.weatherObjectList = weatherObjectList;

        this.weatherList = this.weatherObjectList.get(0).getWeatherList();
        this.weatherTitleList = this.weatherObjectList.get(0).getWeatherTitleList();
    }

    @Override
    public int getCount() {
        return weatherList.size();
    }

    @Override
    public Object getItem(int position) {
        return weatherList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = View.inflate(context, R.layout.weather_item_list, null);
        TextView item = (TextView) view.findViewById(R.id.txtItem);
        TextView subItem = (TextView) view.findViewById(R.id.txtSubItem);

        item.setTypeface(null, Typeface.BOLD);

        item.setText(weatherTitleList.get(position));
        subItem.setText(weatherList.get(position));

        return view;
    }
}
