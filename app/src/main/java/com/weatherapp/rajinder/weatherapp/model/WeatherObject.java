package com.weatherapp.rajinder.weatherapp.model;

import android.os.Parcel;

import java.util.List;

public class WeatherObject {
    private List<String> weatherTitleList;
    private List<String> weatherList;

    public WeatherObject(List<String> weatherTitleList, List<String> weatherList) {
        this.weatherTitleList = weatherTitleList;
        this.weatherList = weatherList;
    }

    protected WeatherObject(Parcel in) {
        weatherTitleList = in.createStringArrayList();
        weatherList = in.createStringArrayList();
    }

    public List<String> getWeatherTitleList() {
        return weatherTitleList;
    }

    public void setWeatherTitleList(List<String> weatherTitleList) {
        this.weatherTitleList = weatherTitleList;
    }

    public List<String> getWeatherList() {
        return weatherList;
    }

    public void setWeatherList(List<String> weatherList) {
        this.weatherList = weatherList;
    }

}
