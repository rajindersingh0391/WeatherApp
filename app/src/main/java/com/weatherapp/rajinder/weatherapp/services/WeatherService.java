package com.weatherapp.rajinder.weatherapp.services;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class WeatherService {
    public static JSONObject getWeatherDetails(Context context, final String zipCode){
        String urlString =  "http://api.openweathermap.org/data/2.5/weather?zip="+zipCode+"&APPID=2ec4f17efca5a3b6b258f326d1cd8756&units=Imperial";
        try{

            URL url = new URL(String.format(urlString));
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            StringBuilder result = new StringBuilder();
            String line="";
            while((line=reader.readLine())!=null) {
                result.append(line).append("\n");
            }
            reader.close();

            JSONObject jsonObject = new JSONObject(result.toString());

            return jsonObject;
        }catch (Exception e){
            Log.i("Myapp", e.toString());
            return null;
        }

    }
}
