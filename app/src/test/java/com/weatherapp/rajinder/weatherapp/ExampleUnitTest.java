package com.weatherapp.rajinder.weatherapp;

import android.content.Context;

import com.weatherapp.rajinder.weatherapp.services.WeatherService;

import org.json.JSONObject;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    //If  jsonObject comes as null, test should fail
    public void testGetWeatherDetails(Context context, final String zipCode){
        JSONObject jsonObject = WeatherService.getWeatherDetails(context, zipCode);
        assertNotEquals(jsonObject,null);
    }
}